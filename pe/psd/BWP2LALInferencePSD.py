import numpy as np
import argparse


p = argparse.ArgumentParser()
p.add_argument("-d", type=str, required=True, metavar="FILE",help="File with the median PSD created by bayeswave_post. It will look something like */ROQdata/0/BayesWave_PSD_H1_post/clean_median_PSD.dat.0")
p.add_argument("-s", type=float, required=True, metavar="FLOAT",help="seglen")
p.add_argument("-i", type=str, required=True, metavar="STR",help="IFO: H1, L1, or V1")

args = p.parse_args() 

BWP_PSD = np.genfromtxt(args.d)
seglen2 = 0.5*args.s

f = open('BayesWave_median_PSD_%s.dat'%(args.i), 'w')
for i in range(len(BWP_PSD[:,0])):
    f.write("%f %e\n"%(BWP_PSD[:,0][i],(BWP_PSD[:,1][i])/seglen2))
f.close() 
