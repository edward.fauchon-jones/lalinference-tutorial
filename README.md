# lalinference-tutorial

This repo contains a set of resources for a lalinference tutorial
describing how to reproduce the results of the rerun analysis for
GW150914.

## Installation
    
Clone the repository 

```bash
https://git.ligo.org/edward.fauchon-jones/lalinference-tutorial.git
```

If you have an ssh keypair setup then you can clone over ssh

```bash
git@git.ligo.org:edward.fauchon-jones/lalinference-tutorial.git
```

The tutorial requires Jupyter Notebook or JupyterLab to be install to
follow the tutorial that is prepared as Jupyter Notebooks.
    
## Usage

It is recommended to clone this repository on either the ARCCA or CIT
clusters where JupyterLab is already available. You can then go to
their respective JupyterLab URLs

- https://ligo.arcca.cf.ac.uk/jupyter
- https://jupyter.ligo.caltech.edu/

and follow the tutorial without any local setup required. Please open
[`README.ipynb`](README.ipynb) when you are ready to start the
tutorial.

## License

lalinference-tutorial is released under the [MIT license](LICENSE.md).
